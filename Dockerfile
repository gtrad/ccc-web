FROM gitlab-registry.cern.ch/gtrad/my_media_cc8 



#### FULL INSTALLATION AS ROOT
#USER root
#WORKDIR /.streamlit
#WORKDIR /tmp
#ADD ccc-web .
#RUN /usr/bin/python3 -m pip install -r requirements.txt 
#CMD streamlit run main.py --server.port 8080
#EXPOSE 8080
#EXPOSE 4333
#ENV MPLCONFIGDIR=/tmp


USER root
RUN yum install -y python3-devel
USER gtrad_media

#### FULL INSTALLATION IN VENV AS USER
ENV MPLCONFIGDIR=/tmp
ENV HOME=/tmp
USER gtrad_media
WORKDIR /tmp
ADD ccc-web .
RUN /usr/bin/python3 -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
CMD . venv/bin/activate && exec streamlit run main.py --server.enableWebsocketCompression=false --server.port 8080
USER root
EXPOSE 8080
EXPOSE 4333
EXPOSE 4567
USER gtrad_media


##USER ROOT
## RUN /usr/bin/python3 -m pip install --user -r requirements.txt 
##RUN /usr/bin/python3 -m pip install -r requirements.txt 
## CMD  /usr/bin/python3 -m streamlit run main.py --server.port 8080
##CMD  streamlit run main.py --server.port 8080
#USER ROOT
#EXPOSE 8080
#EXPOSE 4333
#RUN chmod 7777 /.streamlit/*
#USER gtrad_media
#CMD . venv/bin/activate && exec streamlit run main.py --server.enableWebsocketCompression=false --server.port 8080

